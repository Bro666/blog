---
date: "2020-09-21T09:46:00Z"
draft: true
title: Vision for the Future - Steep 0
---

Hello everyone,

Nate gave an excelent talk at the Akademy about how we can konquer the world
and reach new horizons with our software. One of the first steep for Nate is
for the e.V. to start paying more developers to work on core KDE technologies.

I believe there is an even more important steep before, finding the money to
pay the developers. Current incomes of the e.V. are €183.883 while the expense
is €258.851. No need to be good in math to understand that we are losing money.
This is normal because we were hording to much money for a long time without
spending it, but this is still not a substainable situation and if we start
paying developers we will need to find even more moneys.

KDE's current incomes come from donations from companies (€60 000) but also from
doing one time donations (€35 000) or recurring donations (€9000) from
individuals. There is also a lot of companies that help by sponsoring the
Akademy and other events organized by KDE. The number above are my prognostics
looking at the current trends.

Many thanks to all this wonderful people donating money to the e.V. but this
is unfortunally not enough and if we want to start paying developers we will
need to change our fundraising strategy radically.

One of the reasons why we don't raise as much fund as we could is because of
the failing of our recurring donation system. When the money raised through the
one-time donation system increase by 50% in just one year, the recurring
donation system lost 10% of its donors at the same time.

Currently we are using CiviCRM as our donation system, CiviCRM is a Customer
Relationship Management for non-profit and non-gourvernmental groups. CiviCRM
is a complex web application and has many features for non-profit, we are
currently using the CiviContribute extension to manage the recurrent donations.

Unfortunally, like the numbers are telling, this doesn't work well. We have
technical difficulties with the system. The problems are not new and there was
multiple attends to fix then by hiring CiviCRM specialized consultants. KDE e.V.
recently hired new consultants, and I'm crossing my fingers that this time it
will work. This would at least solve some problems for the time being.

Another problem to solve is the design of the website: to make
it short, [relate.kde.org](https://relate.kde.org) is ugly and need a visual
refresh and an update of the content. So I developed a new theme, available
[here](https://invent.kde.org/carlschwan/civicrm-relate-docker/-/tree/main/aether).
It's not perfect but a lot better than the current one and it was quite an
horror story (more on that later).

And the last problem is also how we are positioning our donation system. 
Currently it's a traditional organization membership fee and this is the reason
why we are using CiviCRM. When someone pay €100 per year, they become a KDE e.V.
supporting member. Their donation helps KDE e.V. in its activities (sponsoring
sprints, servers, ...). This works if we want KDE to remain a small and
traditional organization developing software as an hobby, but I don't think this
is our goal. Our vision is:

"A world in which everyone has control over their digital life and enjoys
freedom and privacy."

And to archieve this vision, we need to grow, get more people involved, making
sure that people can make a living by contributing to KDE and also contribute
to the less fun area of KDE (thing that nobody care about but are really
important like accessibility).

I believe that if we were to communicate more clearly how by donating, we are
able to improve our software and moving forward with our vision, it should
encourage more people to donate.

Moving forward I don't think CiviCRM is the solution for KDE. I'm quite happy
that the immediate problems will hopefull get resolved soon but we need a
better long term solution.

CiviCRM require constant maintainance and since the buissness model is having
a network of consultants, it wasn't developed with easy of use in mind. For
example it doesn't use the standard php package manager `composer`, but require
instead downloading each package manually and keeping track of the version
manually.

CiviCRM use a the infamous Drupal 7 theming engine for rendering the pages.
It means that instead of working with a templating engines like 99% of the web
frameworks, Drupal 7 works with hooks, hooks are function that get called
when rendering a certain portion of a page. This creates a very inflexible
way to create a website and with a some part of the layout that can only be
changed using JavaScript. CiviCRM doesn't help by creating a dumping its forms
without anyway to customize the appareance unless you again use JavaScript to
change the HTML dynamically.

The good news is that CiviCRM will soon switch to Drupal 8 and use an normal
templating engine, but it's also means that the theme will need to be
rewritten and data migrated. And Drupal 8 will be eol in November 2021, so we
will need to rewrite two times our theme in 1 year.

There are many other parts of CiviCRM that I think are not great, and
if someone interested in hearing more I would happily give you more details.

But more importantly I don't think CiviCRM is adapted for our needs of a simple
but also super efficient donation system.

Because of this I experimented on a new system based on the Blender Fund
project. Blender Fund was developed by Blender devs and allowed Blender to
raise enough money to employ many Blender devs to work full time on Blender.
This allowed Blender to become a leading 3D creation suite. See
https://www.youtube.com/watch?v=Jcl3--cbULk. Blender Fund is licensed under
GPL and is based on Django.

A demo of my proposed system can be found here: https://fund.carlschwan.eu/
and the repo https://invent.kde.org/websites/fund-krita-org. A nice thing
about the system is that it is easy to switch the skin for other projects
(e.g. https://krita-fund.carlschwan.eu).

An interesting feature is that it is already integrated with the future
replacement of KDE Identity (code name MyKDE). So you can already login in this
demos using your KDE Identity credentials. Another feature is that donators
get a badge that they can display on their profile page in MyKDE.

You can also try to subscribe to payment using this fake credit card:
4111111111111111 and play around.

I strongely believe that this can be a part of the solution, but just a part
of the solution. We also need to change our messaging and make our goals more
clear. These are social problem and not technical one ;)

My plans for a migration would be to run the old CiviCRM system and the new
one at the same time, encourage the CiviCRM donors to renew their subscriptions
in the new system and shutdown CiviCRM then we decide it's not worth supporting
anymore. This is why it is so important that CiviCRM still works and the recent
efforts to make it somewhat work again won't go to waste.

Please let me know if this is worth putting more effort on it or if the current
system is good enough. I feel like this should be a communit decision, since
fundraising is everyone's concern.

Regards,
Carl Schwan
KDE Web lead developer
