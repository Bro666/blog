---
layout: page
title: About
---

My name is Carl Schwan and I'm 22 years old.

I grow in a French and German family and I'm currently living in Saarbrücken
(Germany), before that I lived in Nancy (France) for 10 years and in a small
village in the Auvergne (also France). So please feel free to contact me in French,
German or English.

In my free time, I contribute to the [KDE](https://kde.org) project, there
I'm maintaining some websites, helping with the promotion and occasionally
work on the documentation and development of the applications.

My other hobbies are cycling and cooking.

You can contact me at [carlschwan@kde.org](mailto:carlschwan@kde.org) or
[carl@carlschwan.eu](mailto:carl@carlschwan.eu).
