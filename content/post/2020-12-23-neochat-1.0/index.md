---
date: "2020-12-23T00:00:00Z"
title: Announcing NeoChat 1.0, the KDE Matrix client
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Blog
  - NeoChat
tags:
  - NeoChat
  - KDE
image: neochat-drawer-open.png
comments:
  host: mastodon.technology
  username: kde
  id: 105429538591797190
---

Matrix is an instant messaging system similar to Whatsapp or Telegram, but
uses an open and decentralized network for secure and privacy-protected 
communications. NeoChat is a visually attractive Matrix client that works on 
desktop computers and mobile phones.

## Convergence

NeoChat provides an elegant and convergent user interface, allowing 
it to adapt to any screen size automatically and gracefully.

![Image](https://www.plasma-mobile.org/img/post-2020-11/neochat-drawer.png)

This means you can use it both on desktop computers, where you might want to 
have a small bar on the side of your screen, but you can also enjoy NeoChat 
on Plasma Mobile and Android phones.

In fact, NeoChat will be installed by default on the PinePhone KDE edition and 
we offer a [nightly Android 
version](https://binary-factory.kde.org/view/Android/job/Neochat_android/)
too. The Android version is for the moment experimental and some
features, like file uploading, don't work yet.

## Features

NeoChat provides a timeline with support for simple messages and also allows 
you to upload images and video and audio files. You can reply to messages and 
add reactions.

NeoChat provides all the basic features chat application needs: apart from 
sending and responding to messages, you can invite users to a room, start 
private chats, create new rooms and explore public rooms.

![Start a chat dialog](start-a-chat-dialog.png) ![Explore rooms](explore-rooms.png)

Some room management features are also available: You can ban or kick 
users, upload a room avatar and edit a room's metadata.

![Room Setting dialog](room-setting-dialog.png) ![Message detail dialog](neochat-chat-context.png)

The room view contains a sidebar that is automatically displayed on wide 
screens, but also appears as a drawer on smaller screens. This sidebar contains 
all the information about the room.

![Invitation UI](invite.png) ![Multiaccount Support](multiaccount-support.png) ![Sidebar](sidebar.png)

A lot of care has been put into making NeoChat intuitive to use. For 
example, copying with Ctrl+C and dragging and dropping images just work; and 
the text field gets autofocused so that you are never writing into the void. 
NeoChat also integrates an emoji picker, letting you use the greatest invention 
of the <del>21st century</del>. (Note: someone in Mastodon pointed out that
Emojis are from the 20th century and appeared in 1997 in Japan.)

## Image Editor

NeoChat also includes a basic image editor that lets you crop and rotate 
images before sending them. The image editor is provided by a small library 
called KQuickImageEditor.

<div class="embed-responsive embed-responsive-16by9">
  <video src="neochat-2020-12-16_10.35.35.webm"
         autoplay="true" muted="true" loop="true"></video>
</div>

This library for the moment doesn't have a stable API and is released together 
with NeoChat.

## Why Matrix and NeoChat

Matrix is an open network for secure and decentralized communication. This is an 
initiative that is very much aligned with KDE's goals of creating an open 
operating system for everybody. This is why we need a Matrix client that 
integrates into Plasma and thus NeoChat was born.

NeoChat is a fork of Spectral, another QML client, and uses the 
libQuotient library to interact with the Matrix protocol. We would like to send 
out a huge thank you to these two projects and their contributors. Without 
them, NeoChat wouldn't have been possible.

NeoChat uses the [Kirigami framework](https://develop.kde.org/frameworks/kirigami)
and QML to provide an elegant and convergent user interface.

## Translations

NeoChat is fully translated in English, Ukrainian, Swedish, Spanish, Portuguese,
Hungarian, French, Dutch, Catalan (Valencian), Catalan, British English,
Italian, Norwegian Nynorsk and Slovenian. Thanks a lot to all the translators
and if NeoChat is not available in your native language consider joining [KDE
localization team](https://community.kde.org/Get_Involved/translation).

## What is Missing

For the moment, encryption support is missing and NeoChat doesn't support video 
calls and editing messages yet either. Both things are in the works.

We are also missing some integration with the rest of the KDE applications,  
like with [Purpose](https://invent.kde.org/frameworks/purpose), which will 
allow NeoChat to be used to share content from other KDE applications; and 
with [Sonnet](https://invent.kde.org/frameworks/sonnet), which will provide 
spellchecking features.

The fastest way to implement these deficiencies is to get involved! The 
NeoChat team is a friendly group of developers and Matrix enthusiasts. Join us 
and help us make NeoChat a great Matrix client! You can join us at
[#neochat:kde.org](https://matrix.to/#/#neochat:kde.org). We also participate
to Season of KDE, so if you want to get mentored on a project and at the end
get a cool KDE T-Shirt, feel free to say hi.

## Tarballs

Version 1.0 of NeoChat is availabe [here](https://download.kde.org/stable/neochat/1.0/neochat-1.0.tar.xz),
kquickimageeditor 0.1.2 is availabe [here](https://download.kde.org/stable/kquickimageeditor/0.1/kquickimageeditor-0.1.2.tar.xz).
Both packages are signed with my gpg key [14B0ED91B5783415D0AA1E0A06B35D38387B67BE](/gpg.html).

<del>A Flathub release will hopefully be released in the next few days. We will
update this post when it is available.</del>

The Flathub version is [available](https://flathub.org/apps/details/org.kde.neochat).
