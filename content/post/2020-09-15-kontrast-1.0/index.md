---
date: "2020-09-15T09:46:00Z"
locale: en
title: Presenting Kontrast
image: https://cdn.kde.org/screenshots/kontrast/kontrast.png
hideHeaderImage: true
---

Kontrast is a contrast checker available for desktop and mobile devices. You can
use Kontrast to choose background and text color combinations for your website or
app that your users will find easy to read. Kontrast can help you improve the
accessibility for your site or app for people with vision problems.

Kontrast won't catch all the problems, but it should still be very helpful to catch
many issues early on, when designing your interface.

I released the first version of Kontrast earlier this month.

![Kontrast main page](https://cdn.kde.org/screenshots/kontrast/kontrast.png)

Another big feature of Kontrast is the possibility to generate random color
combinations with good contrast. These colors can be saved in the application itself,
so that you can keep a particularly good color combination for later use.

Kontrast is available for the Linux desktop, [Plasma Mobile](https://plasma-mobile.org) and
there is also a Beta version for Android.

This application is built using the excellent [Kirigami](https://develop.kde.org/frameworks/kirigami)
framework.

![Kontrast mobile view](https://cdn.kde.org/screenshots/kontrast/kontrat_mobile.png)

You can download and install Kontrast from [Flathub](https://flathub.org/apps/details/org.kde.kontrast) and
a nightly build is also available [in binary factory](https://binary-factory.kde.org/job/Kontrast_android/).

The tarball is available [here](https://download.kde.org/stable/kontrast/kontrast-1.0.2.tar.xz)
and is signed with my gpg key [14B0ED91B5783415D0AA1E0A06B35D38387B67BE](/gpg.html).

## Future Improvements

Future plans include improving the Android build, possibly release a stable
build of the Android version and also create a QtQuick-based color picker
that is better integrated with the application.
