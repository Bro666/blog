---
title: KQuickImageEditor 0.2 released
date: "2021-10-02T10:00:35Z"
image: resize.png
hideHeaderImage: true
---

I’m happy to announce the 0.2 release of KQuickImageEditor. KQuickImageEditor is a QML library providing basic image editing functionality. It is currently used by [Koko](https://apps.kde.org/koko), [NeoChat](https://apps.kde.org/neochat) and [Maui Pix](https://apps.kde.org/pix).

<div class="embed-responsive embed-responsive-16by9">
  <video class="embed-responsive-item" src="crop.webm" controls allowfullscreen></video>
</div>

In this release, Noah Davis worked on improving the usability and design of the the existing croping feature. It now features more touch friendly handles that are consistently looking with the one from Spectacle.

The old ReziseRectangle component is now deprecated and will be removed in the next version of KQuickImageEditor.

KQuickImageEditor 0.2 also brings a new functionality also contributed by Noah: image resizing.

![Image rezising component](resize.png)

## Packager section

A tarball of this release is available at [download.kde.org/stable/kquickimageeditor/kquickimageeditor-0.2.0.tar.xz](https://download.kde.org/stable/kquickimageeditor/kquickimageeditor-0.2.0.tar.xz). The package is signed with my gpg key [14B0ED91B5783415D0AA1E0A06B35D38387B67BE](https://carlschwan.eu/gpg-key).