---
title: More KDE apps
date: "2021-12-18T10:00:35Z"
image: talk.png
tags:
  - KDE
---

KDE is all about the Apps is one of the current goals of the KDE community. Since this goal was choosen and announced in 2019 (remember it’s last year that was ‘normal’), I got a new hobby and it’s improving our apps infrastructure (e.g. apps.kde.org) and developing new KDE apps.

I already talked previously about NeoChat (a Matrix client), Kontrast (a contrast checker), Koko (an image viewer) and Pikasso (a simple Rust-powered drawing app for the Plasma Mobile).

Since then, I have developed a few new applications.

## Tokodon

I’ve been using Mastodon since years and very much like the idea behind it and the fediverse in general. It’s a social network that is not controlled by a single big organization with dubious ethics but instead is controlled by many and everyone can decide to host their own instance.

Tokodon is a simple client, I developed for this protocol. This app is still in progress and currently it allows to browser the different timelines (home, federated, global), to look at user profiles, send posts with images and multiple accounts support.

The current most prominent missing features are the notification view, account settings and support for some types of post (e.g. videos, polls, …).

If someone is interested to contribute, I will try to mentor a season of KDE project with the goal to implement a notification view to Tokodon (or any of the other big missing features).

![Tokodon](tokodon-login.png)

![Tokodon mobile view](tokodon-home.png)

![Tokodon desktop view](tokodon-desktop.png)

[Link to the code](https://invent.kde.org/network/tokodon)

## Hash-o-matic

Hash-o-matic is a straightforward GUI that allows the user to calculate checksums of files without using a terminal utility. It’s a simple application that I mainly developed in two evenings. It allows comparing two files, checking if a file matches a checksum and generating checksums for files.

Aside from that, special care was made to integrate it neatly with KDE. A global menu is exposed to global menu users, in Dolphin you can use a right click action to open a file with Hash-o-matic, the app remembers it’s size and position like most KDE apps and finally, drag-and-dropping files from other apps also works!

I would say that the app is now mostly done, and the only remaining feature that might be added is to allow customizing the hashing algorithms used. Currently, only md5, sha1 and sha512 are available.

![Hash-o-matic](hashomatic.png)

[Link to the code](https://invent.kde.org/carlschwan/hash-o-matic)

## Whale

Whale is actually a very simple file explorer using Kirigami, that I have been building for fun. It’s pretty rudimentary. Currently it allows browsing directories and I forked the plasma folder view to implement a right-click menu and file selections. It still needs a lot of work, but immediate plan is too at some point reuse the code I’m writing for Whale and reuse it for Koko (the Plasma Mobile image viewer) folder view.

If someone wants to help or just look at the code, you can find it on my [invent profile](https://invent.kde.org/carlschwan/whale/).

<figure style="max-width:600px;margin-left:auto;margin-right:auto" class="embed-responsive embed-responsive-16by9"><video src="https://carlschwan.eu/2021/12/18/more-kde-apps/whale.mp4" loop="" autoplay="" muted=""></video><figcaption style="margin-top:3rem">Whale, selection</figcaption></figure>

## Kalendar

This summer, I mentored for GSoC for extending a prototype I built earlier this year of a Kirigami-based calendar app using Akonadi as backend. You can read more about it on Claudio Cambra’s blog, who is now maintaining the app.

![Kalendar month view](kalendar.png)

## Pelikan

In a similar fashion as Kalendar, I also started working on a small mail client using Kirigami and Akonadi. It currentl kinda works for viewing emails, but unfortunately many aspects of KDE PIM libraries related to emails depend too much on QtWidgets and are as such difficult to integrate in a QML application. I started separating the UI code from the buisness logic, but this is an hardeous task and I’m missing time and motivation.

Here is the current state.

![Pelikan](pelikan.png)

[Link to code](https://invent.kde.org/carlschwan/quickmail/)

## Nextcloud talk

I have been using Nextcloud Talk a lot in recent months, so I started writing yet another chat client in Kirigami for it. It’s currently a soft-fork of the sailfishOS client written by a colleague.

The current feature set is very basic and most of the QML frontend code has been based on the code we wrote with Tobias for NeoChat.

![Nextcloud Talk](talk.png)

There is still a lot of work required to fix all the issues and get it to a state where it’s usable for daily usage.

[Link to the code](https://github.com/CarlSchwan/talk-desktop)

## Supporting

If you want to support the development of these applications, I strongly encourage you to donate to the KDE community. These donations help us keep our infrastructure running, including our GitLab instance, our websites, and more. You can donate at https://kde.org/community/donations/.