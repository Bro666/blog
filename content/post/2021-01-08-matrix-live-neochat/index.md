---
title: Matrix Live about NeoChat
date: "2021-01-08T17:00:00Z"
description: Matrix Live about NeoChat with Tobias and myself
image: picture.png
hideHeaderImage: true
categories:
 - Free Software
comments:
  host: linuxrocks.online
  username: carl
  id: 105521588834702801
---

I was recently interviewed with Tobias by the Matrix folks
about NeoChat as part of Matrix Live. You can read the integrality
of the Matrix update for this week
[here](https://matrix.org/blog/2021/01/08/this-week-in-matrix-2021-01-08).

{{< youtube BSJY79qN1No >}}
