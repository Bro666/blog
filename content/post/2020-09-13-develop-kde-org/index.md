---
date: "2020-09-13T13:50:00Z"
locale: en
title: Develop.kde.org
image: homepage.png
---

The [new website](https://develop.kde.org/) for kde developement information was deployed during the
[Akademy](https://akademy.kde.org/2020) and contains many information about
building awesome stuff using KDE tools and large collection of Qt based
libraries.

The long term goal of this new website is to increase the first and third
parties use of the KDE Frameworks and development tools. To achieve this goal,
this website will provide high quality and complete documentation about the
usage of the KDE Frameworks and other libraries (a quite ambitious goal I know),
but also provide marketting content for the libraries to offer them a bigger
visibility in the internet.

The more short term and more realistic goal is to import the existing tutorials 
available from various places ([techbase](https://techbase.kde.org),
[the framework book](https://share.kde.org/s/jqpMS4gPs3HoxLN), the plasma mobile
docs and other more hidden places. And more importantly while importing the
content, also update and improve it and allow other in the community to review
the content for correctness. Another big task is to better organize the content
in logical sections.

## Behind the scene

The website is powered by the Hugo static site generator and by a fork of the
[docsy](https://github.com/google/docsy/) theme. The modifications to the theme
are the support of gitlab web editor, the integration of KDE branding and support
of linking to [api.kde.org](https://api.kde.org) class and function with macros.
In term of design, I'm not entirely satisfied yet but for a first it's good
enough and because I know developers like dark themes, the website also supports
a dark via `prefers-color-scheme: dark`.

All these changes allow us to provide a custom website tailored to our needs
(git based code review, web interface to edit the page if needed, custom design,
api.kde.org integration and good separation between the content and the layout 
of the page).

Future planned improvements to the infrastructure are:

* Add a `.gitlab-ci.yml` to test most of the code examples, to make sure the code
examples still compiles and there is no regressions.

## Get Involved

You can find many tasks in [Invent](https://invent.kde.org/documentation/develop-kde-org/-/issues),
porting old tutorials and improving them can be a good way to learn them or
better understand them.

You can also review [open merge requests](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests)
and look if the information are correct.

Thanks to (in no particular order) Paul Brow, Suraj Kumar Mahto, Tobias Fella,
David Barchiesi, Han Young, Jonah Brüchert, Nicolas Fella, Nate Graham, Cyril
Rossi, Ahmad Samir and Kevin Ottens for their help contributing new content 
or/and reviewing open merge requests.

