---
title: NeoChat 1.1
date: "2021-02-23T20:00:00Z"
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Blog
  - NeoChat
tags:
  - NeoChat
  - KDE
image: multimodal.png
comments:
  host: mastodon.technology
  username: kde
  id: 105782079763058379
---

Exactly 2 months after [NeoChat 1.0](/2020/12/23/announcing-neochat-1.0-the-kde-matrix-client/), the NeoChat team is happy to announce a new release of NeoChat. NeoChat is a native client for the decentralized communication network Matrix. 

Aside from the many bug fixes, performance improvements, and subtle appearance improvements, NeoChat 1.1 brings many new features that will make your experience with it more convenient.

Thanks to the work of Hannah, Nicolas, and Tobias, this release also brings NeoChat to many more platforms. Nightly builds of NeoChat are now available on [Android](https://binary-factory.kde.org/job/Neochat_android/), [Flatpak](https://binary-factory.kde.org/job/Neochat_x86_64_flatpak/), [AppImage](https://binary-factory.kde.org/job/NeoChat_Nightly_appimage/), [macOS](https://binary-factory.kde.org/job/NeoChat_Nightly_macos/) and [Windows](https://binary-factory.kde.org/job/NeoChat_Nightly_win64/). Not all of them are considered production ready, but we hope to improve the support for them in future release.

## New features

### First launch experience improvements 

Probably the highlight of this release is the completely new login page. It detects the server configuration based on your Matrix Id. This allows you to login to servers requiring Single Sign On (SSO) (like the Mozilla or the incoming Fedora Matrix instance). 

Servers that require agreeing to the TOS before usage are correctly detected now and redirect to their TOS webpage, allowing the user to agree to them instead of silently failing to load the account.

![Login Page](login.png)

### Stickers

Everyone loves cute stickers, so now NeoChat supports displaying them too. We don't yet support sending them.

![Viewing a Sticker](sticker.png)

### Editing messages 

Editing messages is another popular feature of every IM client. NeoChat is now able to show edited messages correctly and also make it possible to edit your messages. The behavior is the same as in Element. 

![Editing messages](editing.png)

### Multimodal mode 

It is now possible to open a room into a new window. This allows you to view and interact with multiple rooms at the same time.

![Multimodal mode](multimodal.png)

### Commands

We added a few commands to NeoChat. Previously you could use `/me` and `/rainbow`, and we added a few mores: `/shrug`, `/lenny`, `/rainbowme`, `/join`, `/invite`, `/part`, `/ignore`, `/unignore`.

### Plasma integration 

We improved the Plasma integration a bit. Now the number of unread messages is displayed in the Plasma Taskbar. It is using the `com.canonical.Unity.LauncherEntry` DBus protocol, so that should be reasonably supported across desktops.

## Tarballs

Version 1.1.1 of NeoChat is availabe [here](https://download.kde.org/stable/neochat/1.1.1/neochat-1.1.1.tar.xz).
The package is signed with my gpg key [14B0ED91B5783415D0AA1E0A06B35D38387B67BE](/gpg.html).

For users, a [Flathub version](https://flathub.org/apps/details/org.kde.neochat) will be updated shortly.
