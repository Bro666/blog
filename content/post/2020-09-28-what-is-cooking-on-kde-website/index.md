---
author: Carl Schwan
date: "2020-09-28T16:30:35Z"
locale: en
title: What is cooking on KDE websites this month (September)?
image: season1.png
hideHeaderImage: true
---

This month a few cool things happened to the KDE websites. 


* The wiki instance we use was migrated to MediaWiki 3.34 (latest LTS version), bringing a few improvements in the translations module and fixing the problem where translated pages couldn't be moved around. The commenting plugin was sadly discountinued in this version and instead the Echo extension was added which provides a way to ping people.

* Another improvement related to the wikis is the creation of a small module allowing user to authenticate with the Identity replacement. The source code can be found [here](https://invent.kde.org/websites/mykde-mediawiki). This brings us a step closer to the replacement of identity.kde.org.

* This month, a completely rewritten backend for Season of KDE was merged. This was part of the work Anuj did during GSoc this year. It is not yet deployed into production, but I host a [demo](https://season.carlschwan.eu) so you can try it. The new features allow for a mentor to comment on proposals, and mentees can now use markdown when writing a proposal. The new system now uses Symfony, one of the biggest PHP frameworks, and is integrated in MyKDE too, so mentors and mentees get a badge after completing SoK. The admin interface was also significantly improved. Great work Anuj!

![Season homepage](season1.png)

![Season admin](season2.png)

* And while at it, please consider mentoring for this year's SoK program. It is a good opportunity to bring new blood to KDE. [community.kde.org/SoK/Ideas/2021](https://community.kde.org/SoK/Ideas/2021).

* The new developer documentation website also got a few updates and now contains a complete tutorial about how to write a [Plasma Widget](https://develop.kde.org/docs/plasma/). Thanks to Zren for this contribution.

* Elisa, everyone's favorite music player, also got a new website developed by Anubhav Choudhary and Nikunj Goyal. You can check it out at [elisa.kde.org](https://elisa.kde.org).

* Subtitle Composer, a subtitle editor, also got a new website developed by Thiago Sueto. You can check it out at [subtitlecomposer.kde.org/](https://subtitlecomposer.kde.org/).

* The [atelier website](https://atelier.kde.org/) was also updated by Lays Rodrigues.

* [KDEMail.net](https://kdemail.net/) now uses the Aether theme.

* I sent a proposal to use a fork of the Blender Fund tool for KDE Fundraising in the community mailing list. You can check out the demo for [KDE](https://fund.carlschwan.eu) and [Krita](https://krita-fund.carlschwan.eu).
