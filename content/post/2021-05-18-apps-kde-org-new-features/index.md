---
title: LAS 2021 and improvements in the applications infrastructure
date: "2021-05-18T13:05:35Z"
image: discover-hero.png
hideHeaderImage: true
categories:
  - Free Software
  - KDE
  - Community
tags:
  - KDE
---

Last week I attended and even gave a small talk to the Linux App Submit (LAS).
LAS is a cross-distro and cross-desktop event around Linux applications. It's
a good place to learn about all the new cool thing making it easier to
build and distribute Linux applications. This motivated me to improve a bit
more the presence of Plasma Mobile applications in Flathub, but also make
various improvements to [apps.kde.org](https://apps.kde.org).

## Plasma Mobile and Flathub

Jonah, Devin and myself started packaging more Plasma Mobile applications
to Flathub. Currently we added [Koko](https://flathub.org/apps/details/org.kde.koko),
[Index](https://flathub.org/apps/details/org.kde.index),
[Vvave](https://flathub.org/apps/details/org.kde.vvave),
[Alligator](https://flathub.org/apps/details/org.kde.alligator) and
[Pix](https://flathub.org/apps/details/org.kde.pix) to Flathub and more
pull request are already open and hopefully soon merged.

## Apps.kde.org

[Apps.kde.org](https://apps.kde.org) also got a few updates. Volker fixed
a regression caused by my [latest rewrite of the website](https://carlschwan.eu/2021/04/18/speeding-up-apps.kde.org/)
that was preventing the application page to show links to F-Droid and the
Play Store when available. This prompted me to also add additional links
to Flathub, but also to the nightly Windows build.

![GCompris is really everywhere](compris.png)

Another small improvements and not yet visible in the UI is that it's now
possible to list all applications available on specific platforms. For
example to see all the applications available on Android, you can use
[apps.kde.org/platforms/android/](https://apps.kde.org/platforms/android/).
Similarly to see all the applications available on the Microsoft Store,
you can use [apps.kde.org/platforms/windows/](https://apps.kde.org/platforms/windows/).

On the Android page, some applications have been removed from the store
and we should probably update the metadata accordingly or bring them back.
Hint: It's a good oportunity to get involved!

The next step is to expose these links in the homepage of the website.

## Release tooling

A while ago, I added support for release information in apps.kde.org.
Unfortunately the `<release>` tag isn't often added in KDE applications
for new release. Currently only a handful of applications like
Krita, KDevelop, NeoChat or Calindori are using them. It's a bit sad because
it allows to link to the release announcements, but also link to the
tarball and other artifacts (`.exe`, `.dmg`, `.AppImage`, ...).
Additionally this information are also displayed on Flathub and Discover.

To improve a bit the situation, for at least the package released by
the release service, I started a [merge request for the release-tools](https://invent.kde.org/sysadmin/release-tools/-/merge_requests/16)
that adds more information to the AppStream file. This includes bugs
fixed in the last version, a link to the tarball and the link to the
announcement. A solution for other types of artifacts still need to
be figured out, but since not all applications in the release service
provide AppImages or Windows/macOS package, it's more complicated.

## Discover

Another application related improvement I have been working is a revamp
of Discover homepage. This is still work in progress and the final visual
result might change, but here is how it currently looks:

![Discover new applications with Discover](discover.png)
