---
date: "2020-03-28T18:00:00Z"
image: aether-sphinx.png
locale: en
title: 'This month in KDE Web: March 2020'
---

This month KDE web developers worked on updating more websites and some progress
was made in a new identity provider and a lot of other exiting stuff and a lot
of background work was also done.

## Updated Websites

* [Calligra](https://calligra.org), the KDE office suite, launched a new website.
This was done during [Season of KDE](https://season.kde.org) by Anuj Bansal. This
replaces the old Wordpress website. [See repository.](https://invent.kde.org/websites/calligra-org)

![Calligra website](/assets/img/calligra.png)

* [Planet KDE](https://planet.kde.org) is an aggregator of all the individual blogs
of KDE contributors. It is the place where you can find technical details about a
change, general KDE news, and blog posts about the life of KDE contributors. The website was
updated and now contains feeds in more languages. (Me: Carl Schwan,
[See repository](https://invent.kde.org/websites/planet-kde-org)).

![Planet KDE Website](/assets/img/planet.png)

## Progress to a new identity service

KDE Identity was and still is a source of pain in the KDE infrastructure. It only
supports OpenLDAP, doesn't provide a great onbording experience and isn't very
flexible.

A new account management service is currently in creation and should be based on a
fork of the successful [Blender ID system](https://id.blender.org). This new system
will be based on OAuth2.

This month the homepage of the new identity service was updated to follow KDE branding (Me: Carl Schwan,
[see commit](https://invent.kde.org/websites/my-kde-org/-/merge_requests/7))
and the settings are now using environment variables (Lays Rodrigues, 
[see commit](https://invent.kde.org/websites/my-kde-org/-/merge_requests/4/diffs)).

![KDE identity](/assets/img/accounts.png)

But the most significant work was rewriting [season.kde.org](https://season.kde.org)
and making it compatible with the new identity service. This is still in progress and
a few features are missing but everything should be finished soon. (Me: Carl Schwan,
[See repository.](https://invent.kde.org/carlschwan/season-kde-org)).

![Season Website](/assets/img/season.png)

## KDE.org changes

* The [hardware page](https://kde.org/hardware) created last month was updated
to include the Pinebook Pro (Niccolò Venerandi) and now includes hardware specifications
(Me: Carl Schwan).

![hardware page](/assets/img/hardware.png)

## Library updates

* An Aether theme was created for the Sphinx documention engine based on the Read
the Docs theme. This isn't used yet for any website but in the future, it will
be used for [hig.kde.org](https://hig.kde.org) and maybe others. Stay tuned!
(Carson Black, [see repository](invent.kde.org/websites/aether-sphinx))

![Aether sphinx](/assets/img/aether-sphinx.png)

## How you can help

We always need help with websites, fixing papercuts, upgrading old
websites to the new Jekyll/Hugo infrastructure, making sure information on
the websites are up-to-date, creating beautiful home page for your
favorite project and a lot more.

You can join the web team through our [Matrix channel](https://webchat.kde.org/#/room/#freenode_#kde-www:matrix.org)
, our IRC channel (#kde-www) or our [Telegram channel](https://t.me/KDEWeb).

