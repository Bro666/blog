---
date: "2020-04-18T00:00:00Z"
locale: en
ref: kde-org-homepage
title: A new look for KDE.org
image: kde-org-homepage.png
---

Started a long time ago and only finished now, I have the pleasure to announce
the publication of a new homepage for the KDE community. I hope you will enjoy it
as much as I did creating it.

This is only a small preview. Head over to [kde.org](https://kde.org) to see
the complete webpage.

First of all I have removed the carousel. Some studies show that a carousel is
a very bad way to convey information. See "[should I use a carousel](http://shouldiuseacarousel.com/)".

Instead, I've created various sections about KDE. Plasma, the applications,
hardware that is shipped with Plasma, and, more importantly, the KDE Community.
This is just an overview, we have way more projects, but I hope it gives you a
good introduction of the work we, as the KDE Community, have been doing for more then 20
years. If you are interested, you can find all the different products we
create on the [KDE products page](https://kde.org/products).

The new webpage was not only my work, but a collaborative work and I want to
thank in no particular order Ilya Bizyaev, Niccolò Venerandi, Paul Brown, manueljlin,
David Cahalane, Blumen Herzenschein and others for their help proofreading,
suggesting changes and creating the artwork for the hardware section (thanks
again manueljlin).

## Fun fact

Thanks to David Cahalane, I learned that this is the 8th version of the kde.org homepage.
For those interested, I compiled a list of snapshots from archive.org showing
the evolution of the homepage.

 * [29 January 1998](https://web.archive.org/web/19980129141510/http://www.kde.org/index.html) It's older than I am \o/
 * [3 March 2000](https://web.archive.org/web/20000303113442/http://www.kde.org/index.html)
 * [2 March 2001](https://web.archive.org/web/20010302060501/http://www.kde.org/)
 * [23 June 2003](https://web.archive.org/web/20030623221955/http://www.kde.org/)
 * [18 June 2009](https://web.archive.org/web/20090618162051/http://www.kde.org/)
 * [28 February 2013](https://web.archive.org/web/20130228134554/http://www.kde.org/)
 * [26 Jun 2018](https://web.archive.org/web/20130228134554/http://www.kde.org/)
 * [20 April 2020](https://kde.org) 

<div class="embed-responsive embed-responsive-16by9">
  <video class="embed-responsive-item" src="/assets/kdeorg_thru_ages_fast.mp4" controls allowfullscreen></video>
</div>

## Get Involved

There is always more work needed in order to spread KDE all over the world. Creating graphics,
polishing websites, writing announcements are just some of the tasks you can help with. Join the KDE Web and KDE Promo group and
help us let the world know about KDE.

**KDE Web:**

* #kde-www
* [#freenode_#kde-www:matrix.org](https://webchat.kde.org/#/room/#freenode_#kde-www:matrix.org)
* [Telegram](https://t.me/KDEWeb)

**KDE Promo:**

* #kde-promo
* [#kde-promo:kde.org](https://webchat.kde.org/#/room/#kde-promo:kde.org)
* [Telegram](https://t.me/joinchat/AEyx-0O8HKlHV7Cg7ZoSyA)
