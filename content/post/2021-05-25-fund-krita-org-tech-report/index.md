---
title: Tech report of fund.krita.org
date: "2021-05-25T09:35:35Z"
author: Carl Schwan
categories: free software
---

A few weeks ago, the Krita project announced the Krita Dev Fund. This project
scope was the same as the [Blender Dev Fund](https://fund.blender.org). Provide
a stable way to fund the development of Krita via recurring donations.

Since I was the one who helped with the technical bits on the website and I
heard that were are interested FOSS communities that want to deploy something
similar, I decided to write a small tech report about how we did it in Krita.

Luckily for us, when we started researching how to create something similar,
we rapidly discovered that the Blender Fund website itself is
[open source](https://developer.blender.org/source/blender-dev-fund/repository/master/)
and licensed under the same license as Blender: GPL. It made it easy
to reuse the code.

The system is written with Django for the backend and uses Braintree as
a payment provider. The code was built with Blender need in mind and not
as a reusable project that I could use without any change, so a
fork was inevitable. To make the maintenance more manageable, I decided
to try to stay as near as possible to upstream in our Python code so that
it's easy to rebase the Krita fork when needed.

The code is hosted [on invent](https://invent.kde.org/websites/fund-krita-org).

Blender Fund is using its own authentification system ([Blender ID](https://id.blender.org))
using OAuth2. To connect to Blender Id, a separate Django module for
authentification exists [(blender-id-oauth-client)](https://gitlab.com/blender-institute/blender-id-oauth-client).
This could be replaced by a custom implementation using [default Django auth module](https://docs.djangoproject.com/en/3.2/topics/auth/default/)
or one of the many third-party Django ready-to-use authentification modules. In
the case of Krita I just used blender-id-oauth-client with a few
oauth2 parameters changed.

The most significant modifications I made were mainly around the HTML templates and
CSS files. Most of these modifications were about replacing Blender with Krita,
and updating the look of the homepage. We couldn't just use Blender's branding!

There was also quite a bit of work with the layouts of the page
since Krita main CSS theme is using Bootstrap 3 and the Blender Fund website is
using Bootstrap 4 and were was many subtle differences between both versions.

Updating only the templates made it possible to customize the website's look
and edit the content of the emails sends by the system to use Krita billing
information instead of Blender one.

In terms of Python code changed, there was also a few instances of 'Blender'
that got replaced by 'Krita'. But all the other changes than were minors.
For example, I added back the progress bar on the landing page since
Blender removed it from fund.blender.org since it wasn't needed anymore.
Another small change was adding to the homepage the amount of the current donation
from the old system.

A few weeks before launching [fund.krita.org](https://fund.krita.org),
I got a pleasant surprise as I wanted to rebase my prototype on top of
upstream before moving Krita Fund to production. I discovered that the
Blender team moved their internal library that handles all the payments
to a separate library. It instantly made the amount of critical code in
Krita's fork smaller and my life much simpler.

That's it! So if you want to support Krita and help them build the best painting
application in the world and available for everyone, make sure to check out the
[Krita Fund](https://fund.krita.org/).

And if you are enjoying my work all around KDE (Plasma Mobile, NeoChat, websites,
...), feel free to check out my [Liberapay account](https://liberapay.com/Carl).
