---
author: Carl Schwan
categories: KDE webdev
date: "2019-06-23T11:00:00Z"
description: Konsole, KDE's terminal emulator got a new website.
locale: en
title: New website for Konsole
---


Yesterday, [konsole.kde.org](https://konsole.kde.org) got a new website.

![Screenshot of the new konsole website](/assets/img/new-konsole.png)

Doesn't it look nice? As a reminder the old website looked like this.

![Screenshot of the old konsole website](/assets/img/old-konsole.png)

The design is very similar to the [kontact.kde.org](https://kontact.kde.org)
and [kde.org](https://kde.org) websites.

The content could probably still need some improvements, so if you find typos
or want to improve the wording of a sentence, please get in touch with
[KDE Promo](https://community.kde.org/Get_Involved/promotion). The good news is
that you don't need to be a programmer for this.

## Community goal

With Jonathan Riddell, we proposed a new community goal:
[KDE is All About the Apps](https://phabricator.kde.org/T11117).

One part of this goal is to provide a better infrastructure and promotional
material for the KDE applications (notice the lowercase a). I think websites
are important to let people know about our amazing applications.

So if you are maintaining a KDE applications and want a new shinning website,
please contact me. And I will try to setup for you a new websites, following
the general design.

## Technical details

The new website uses Jekyll to render static html. Because the layout and the
design aren't unique to konsole.kde.org, I created a special Jekyll located at
[invent.kde.org/websites/jekyll-kde-theme](https://invent.kde.org/websites/jekyll-kde-theme),
so that only the content and some configuration files are located in the 
[websites/konsole-kde-org](https://cgit.kde.org/websites/konsole-kde-org.git/)
repository. This make it easier to maintain and will make it easier to change
others website in the future without repeating ourself.

This was a bit harder to deploy than I first though, I had problem with
installing my Jekyll theme in the docker image, but after the third or fourth
try, it worked and then I had an encoding issue, that wasn't present on my
development machine.

![Ci konsole website](/assets/img/konsole-ci.png)

## How can I help?

Help is always welcome, the KDE community develops more than 200 different
applications, and even though not all applications need or want a new modern
website, there is tons of work to do.

If you are a web developer, you can help in the development of new website
or in improving the Jekyll theme. Internalization, localization and accessibility
still need to be implemented.

If you are not a web developer, but a web designer, I'm sure there is room
for improvement in our theme. And it can be interesting to have small variations
across the different websites.

And if you are neither a designer nor a developer, there is still tons of work
with writing content and taking good looking screenshots. For the screenshots,
you don't even need to have a good English.

If you have question, you can as always contact me in Mastodon at
[@carl@linuxrocks.online](https://linuxrocks.online/@carl) or with
matrix at @carl:kde.org.

You can discuss this post in [reddit](https://www.reddit.com/r/kde/comments/c4bbym/new_website_for_konsole/) or [mastodon](https://linuxrocks.online/@carl/102322804611731440).

<style>
img, video {
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
    display: block;
}
</style>
