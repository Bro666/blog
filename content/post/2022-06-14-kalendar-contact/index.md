---
title: Kalendar Contact Book - Current Progress
date: "2022-06-14T7:00:35Z"
categories:
  - Free Software
  - KDE
tags:
  - KDE
  - Kalendar
image: contact-view.png
#comments:
#  host: floss.social
#  username: carlschwan
#  id: 106088040287852932
---

During my long train trip to the [Linux App Summit 2022](https://linuxappsummit.org/), I started
working on a contact book feature in Kalendar. There was already a small contact integration in
the event editor to select attendees for an event and I wanted to extend it with a simple contact
info viewer and editor.

When I started it, I was full of hope that this would be a simple task and would be easy to
finish. Unfortunately more than one month later, it's not finished but there is a lot of progress
that I can already show off.

## The Contact View

The contact view is the most immediate visual change that users will notice when starting Kalendar.
It's a new view available in the sidebar and it will display all your contacts synchornized with
Kalendar. It's also feature a search field, to easily find a contact very helpful when you have
many hundreds contats.

![The contact view showing a few contacts](contact-view.png)

Currently not all the properties that an vcard can contains are displayed, but it is easy to add
more of them later on.

Internally, the contact view uses an
[Akonadi::ItemMonitor](https://api.kde.org/kdepim/akonadi/html/classAkonadi_1_1ItemMonitor.html)
so that the changes to the contact are immediately reflected in the view, even if the changes
happened in KAddressBook or were synced in the background from an online service.

## Contact Book Settings

Kalendar has access to the same sources as KAddressBook with for example WebDav (e.g. Nextcloud),
Etesync, Microsoft Exchange Server and local vCard files.

<em>The Google contact provider is still broken due to a massive API change in Google API. It's a
good reminder that open standards are better for the users and the developers
<a href="https://invent.kde.org/pim/libkgapi/-/merge_requests/22">sanity</a>.</em>

![Contact book source settings](settings.png)

## QR Code Sharing

From the contact view, it is also possible generate a QR code. This makes it easy to share one contact
to your phone. If you want to shares and synchronize multiples contacts, it's better to use a
CardDav-based solution like [Nextcloud](https://nextcloud.com).

![QR code sharing](qrcode.png)

## Plasma Applet

After implementing the contact view, with Claudio we decided to try to keep the codebase for the
calendar and contact support mostly seperated from each others. To to so we created a QML plugin
that contains all the contact utility and that can simply be imported with `import org.kde.kalendar.contact 1.0`.

This code seperation helped us develop a Plasma applet integrated inside the system try for the
contact book.

The applet provides an easy way to search for a contact and send them an email or start a call
using KDE Connect.

![Searching in the Plasma applet](search-applet.png)

![An contact book Plasma applet](applet.png)

It's also possible to share with a QR code directly from the Plasma applet.

## Contact Editor

The contact editor turned out more complicated than [planned](https://twitter.com/CarlKDE/status/1528482513085161475)
and is still missing a lot of features.

![The contact editor](contact-editor.png)

Currently, it only allows to edit the name, the phone numbers and emails of a contact. When editing the
name you also have the choice to set each components of the name separately.

![Advanced name options](contact-editor-advanced.png)

There is also handling for the case there the contact was edited in another Akonadi-powered
editor (like KAddressBook), asking the user what to do when detecting multiple concurrent
editing of the same contact.

![Change detection](change-detection.png)

## Contact Group

Kalendar also has support for contact groups. This allows to create a group of contacts with
an associated email address. It's quite helpful when you want to often send mails to a
group of contact.

![Contact Group](contactgroup.png)

You can also edit them, add more contacts and concurrent editing detection is also built-in
the contact group editor.

![Contact Group editing](contactgroupeditor.png)

## Future

There is still a lot of features missings left to implement. For example, contact deletion,
moving/copying a contact to another contact book but also a lot of contact properties need
to be implemented in the contact book.

These features are relatively straigh forward to implement now that the base is here and if
you want to help with the implementation, join our [Matrix room](https://matrix.to/#/#kalendar:kde.org).
We would be happy to guide you.

Hopefully this will all be ready before the 22.08 release.

The Kalendar team is also working on another big component for Kalendar, stay tunned.