---
title: Announcing Season of KDE Selected Projects
date: "2021-01-11T17:00:00Z"
description: Season of KDE offers an opportunity to everyone (not just students) to participate in both code and non-code projects that benefits the KDE ecosystem
comments:
  host: mastodon.technology
  username: kde
  id: 105538546501693289
---

I'm happy to announce the projects selected for Season of KDE. In this
post we will look at some of the successful proposals, but there is a
full list available [here](https://season.kde.org/1/projects/accepted).

## Plasma Mobile

Nikunj Goyal will be working under the mentorship of Devin Lin to implement
a global weather alert system for KWeather using the CAP protocol.
Anjani Kumar will work on a D-Bus deamon for KWeather that will provide
weather data. Han Young will mentor this project.

Rohan Asokan will be working on Kalk and will add a few new features,
including a binary calculator and a scientific mode with more advanced
functions. Han Young will mentor this project.

Yash Walia will be working on producing new default alarm and ringtones
for KClock. Devin Lin will mentor this project.

Sai Moukthik Konduru will be working on a basic task management tool
for Plasma Mobile. This will also be mentored by Devin Lin.

## Plasma

Chirag Goyal will work on the new Plasma Firewall KCM and will make it
possible to share your plasma firewall configuration using the
[KDE Store](https://store.kde.org). This project will be mentored by
Lucas Januario.

Mariam Fahmy Sobhy will add a new backend to Discover for the rpm-ostree
protocol. This backend is required for the new cool immutables distros
like Fedora Kinoite. This project will be mentored by Aleix Pol Gonzalez
and Timothée Ravier.

Raveesh Agarwal will work on improving the network applets with better
support for multiple network adapters. This project will be mentored by
Jan Grulich.

## Promo

The Promo team will mentor a project allowing us to automate the collection
and analysis of the data we get from our social networks. The goal is to be
able to identify how well our announcements and activity on social media networks
are generating engagements.

Rohan Reddy will work on extracting the aggregated data from the various
sources (Mastodon, Twitter, Facebook, website stats...) and Sankalp Das
will work on a data visualization dashboard.

Manav sethi will work on an application which will allow us to post a
message to multiple social networks at the same time.

## Calamares

While not a KDE project, Calamares is participating in Season of KDE too.

Adriaan de Groot will mentor three projects: the first one is porting all the codebase
to Python 3.6 (Hitesh Kumar). The second one consists of porting the documentation
from GitHub Pages to Jekyll. Finally,  Anubhav Choudhary will work on various
smaller tasks required by Calamares.

## Applications

### KDenlive

Vivek Yadav will work on a separate widget for the bin folders, this will
be helpful for projects with a lot of content, allowing them to be viewed
in a separate window. This project will be mentored by Massimo Stella.

### SkanPage and SkanLite

Smit Shivcharan Patil will work on a QML version of SkanLite called SkanPage.
The goal is to port the old QtQuick Control 1 codebase to Kirigami 
and QtQuick Controls 2. This will be mentored by myself (Carl Schwan) and
should provide a way to have multi-page scanning support.

### Peruse

Mahmoud Ahmed Khalil will add support for reading and creating interactive
fiction comic books in Peruse. This will be mentored by Leinir.

### Okular

Pablo Marcos will work on the Okular website porting it to Hugo and featuring
a new design. This will be mentored by me.

### Mark

Mark is an annotation tool for machine learning datasets. Pranav Gade will
add support for video annotation and Jean Lima Andrade will add support for
audio annotation. Both projects will be mentored by Caio Jordão Carvalho.

## Documentation

Claudio Cambra will work on improving the developer documentation in
develop.kde.org. This project will be mentored by myself.

Suraj Kumar Mahto will be working on improving the docs.kde.org website
and will be mentored by Anuj Bansal and me.

## Unaccepted Proposals

Unfortunately, not every proposal was accepted. There are various reasons for
this. Often proposals are submitted, but are not related to anything we do in
KDE and we can't find mentors for them. In other cases, there are many students who
write a proposal for the same project idea and we can only choose one student per task.

However, I would like to remind people that you can still contribute to KDE
without having to participate in Season of KDE. Developers will still review your
patches and community members will still help you to contribute.
