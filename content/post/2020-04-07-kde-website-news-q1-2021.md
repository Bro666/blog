---
title: KDE websites and documentation news (December 2020 - March 2021)
---

This last few months have been focused on improving the website tooling uing the Hugo framework. The base theme that was developed for kde.org was moved to a seperate repository and is now completely reusable for other projects to use. Other than the speed improvement other Jekyll, there are two big improvements in using Hugo instead of Jekyll for the KDE websites: the translation system is a lot more mature and by using go modules it is easier to share assets, scss modules, templates and translations between websites.

The new Hugo theme was quickly made of use inside the Kate website making it looks nicer and more consistent with the other KDE websites.

Phunh has done a lot of cleaning in the kde.org hugo codebase to simplify it and reduce the amount of duplicate content.

Phunh then ported the planet.kde.org website to use Hugo. The most notable user visible change is that there is now a small tool on the left to navigate between articles. Aside from that the localized feed page is now completely translatable.

Similarly I made plasma-mobile.org switch to Hugo and I'm slowing making every pages translatable. I also added in the homepage a list including most of the Kirigami/Maui applications. This list is provided by apps.kde.org, so that it remains up to date and is localized. Help would be welcome around updating the rest of the plasma-mobile.org website to make it more up to date with the development of the project.

Another big news is that Pablo Marcos finished his SoK project and ported the okular.kde.org website to Hugo and the current KDE theme. It's looking great.

In term of documentation, Clau Cambra improved his Kirigami intruduction and updated the Configuration-related articles. I wrote a new article about how to use [Akonadi and Kirigami to build a mail viewer](https://develop.kde.org/docs/akonadi/using_akonadi_applications/) and imported a few old KWin and Plasma tutorial from techbase to develop.k.o. This include the old [KWin Window Switcher tutorial](https://develop.kde.org/docs/plasma/windowswitcher/), the [KWin scripting guide](https://develop.kde.org/docs/plasma/kwin/) and the [Plasma theme tutorial](https://develop.kde.org/docs/plasma/theme/).


