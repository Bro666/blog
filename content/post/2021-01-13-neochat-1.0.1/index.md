---
author: Carl Schwan
categories:
 - Free Software
 - KDE
 - NeoChat
date: "2021-01-13T11:35:35Z"
title: NeoChat 1.0.1, first bugfix release
image: picture.png
---

This version fixes several bugs.

* NeoChat doesn't require a .well-know configuration in the server to work. (Tobias and Kitsune)
* Edited messages won't show up duplicated anymore. (Carl)
* NeoChat now ask for consent to terms and conditions if required instead of displaying nothing. (Tobias)
* Users avatar in the room list are now displayed correctly. (Carl)
* Fix image saving (Tobias)
* Various graphic glitches have been fixed. (Various authors)

Tarball is available in [download.kde.org](https://download.kde.org/stable/neochat/1.0.1/neochat-1.0.1.tar.xz).
